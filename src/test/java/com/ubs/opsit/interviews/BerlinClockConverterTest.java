package com.ubs.opsit.interviews;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Berlin Clock implementation unit tests
 * 
 */
public class BerlinClockConverterTest {

    private final Logger         LOGGER    = LoggerFactory.getLogger(BerlinClockConverterTest.class);

    private static final String  SCENARIO1 = "Y\nOOOO\nOOOO\nOOOOOOOOOOO\nOOOO";
    private static final String  SCENARIO2 = "O\nRROO\nRRRO\nYYROOOOOOOO\nYYOO";
    private static final String  SCENARIO3 = "O\nRRRR\nRRRO\nYYRYYRYYRYY\nYYYY";
    private static final String  SCENARIO4 = "Y\nRRRR\nRRRR\nOOOOOOOOOOO\nOOOO";

    private BerlinClockConverter testclass;

    @Before
    public void setUp() {
        testclass = new BerlinClockConverter();
        LOGGER.info("testclass has initialized");
    }

    @Test
    public void testScenario1() {
        String actual = testclass.convertTime("00:00:00");
        Assert.assertEquals(SCENARIO1, actual);
    }

    @Test
    public void testScenario2() {
        String actual = testclass.convertTime("13:17:01");
        Assert.assertEquals(SCENARIO2, actual);
    }

    @Test
    public void testScenario3() {
        String actual = testclass.convertTime("23:59:59");
        Assert.assertEquals(SCENARIO3, actual);
    }

    @Test
    public void testScenario4() {
        String actual = testclass.convertTime("24:00:00");
        Assert.assertEquals(SCENARIO4, actual);
    }
}

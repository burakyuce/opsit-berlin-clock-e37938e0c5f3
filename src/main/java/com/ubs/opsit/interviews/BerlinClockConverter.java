package com.ubs.opsit.interviews;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Berlin Clock implementation
 * 
 */
public class BerlinClockConverter implements TimeConverter {
    private static final Logger LOGGER = LoggerFactory.getLogger(BerlinClockConverter.class);

    private static final String OFF    = "O";
    private static final String RED    = "R";
    private static final String YELLOW = "Y";

    @Override
    public String convertTime(String aTime) {

        LOGGER.info("aTime:{}", aTime);

        String newLine = "\n";
        String[] split = aTime.split(":");

        int hours = Integer.valueOf(split[0]);
        int minutes = Integer.valueOf(split[1]);
        int seconds = Integer.valueOf(split[2]);

        return new StringBuffer().append(getTopLight(seconds))
                                 .append(newLine)
                                 .append(getFirstRow(hours))
                                 .append(newLine)
                                 .append(getSecondRow(hours))
                                 .append(newLine)
                                 .append(getThirdRow(minutes))
                                 .append(newLine)
                                 .append(getBottom(minutes))
                                 .toString();

    }

    /**
     * Top light should be blink every two seconds
     */
    private String getTopLight(int seconds) {
        return seconds % 2 == 0 ? YELLOW : OFF;
    }

    /**
     * Top row has 4 lamps and each one represents 5 hours
     */
    private String getFirstRow(int hours) {

        int onLamps = hours / 5;

        String out = designLamps(onLamps, 4, RED);

        LOGGER.info("First row:{}", out);

        return out;
    }

    /**
     * Second row has 4 lamps and each one represents 1 hour
     */
    private String getSecondRow(int hours) {

        int onLamps = hours % 5;

        String out = designLamps(onLamps, 4, RED);

        LOGGER.info("Second row:{}", out);

        return out;
    }

    /**
     * Third row has 11 lambs and each one represents 5 minutes
     */
    private String getThirdRow(int minutes) {

        int onLamps = minutes / 5;

        String out = designLamps(onLamps, 11, YELLOW);

        out = out.replace("YYY", "YYR");

        LOGGER.info("Third row:{}", out);

        return out;
    }

    /**
     * Bottom row has 4 lamps and each one represent 1 minute
     */
    private String getBottom(int minutes) {

        int onLamps = minutes % 5;

        String out = designLamps(onLamps, 4, YELLOW);

        LOGGER.info("Bottom row:{}", out);

        return out;
    }

    /**
     * Turn on the lamps
     */
    private String turnOnTheLamps(int onLamps, String color) {
        String out = "";
        for (int i = 0; i < onLamps; i++) {
            out = out.concat(color);
        }
        return out;
    }

    /**
     * Turns of the remaining lamps
     */
    private String turnOffTheLamps(int onLamps, int totalLamps) {
        String out = "";
        for (int i = 0; i < totalLamps - onLamps; i++) {
            out = out.concat(OFF);
        }
        return out;
    }

    /**
     * Design whole row
     */
    private String designLamps(int onLamps, int totalLamps, String color) {

        String out = "";

        out = out.concat(turnOnTheLamps(onLamps, color));
        out = out.concat(turnOffTheLamps(onLamps, totalLamps));

        return out;
    }

}
